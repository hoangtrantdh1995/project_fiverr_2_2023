import "./sass/sass.scss";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout/Layout";
import HomePages from "./HomePages/HomePages";
import JobTitle from "./HomePages/JobTitle/JobTitle";
import JobDetail from "./HomePages/JobDetail/JobDetail";
import HomePageAdmin from "./Admin/HomePageAdmin/HomePageAdmin";
import LayoutAdmin from "./HOC/LayoutAdmin/LayoutAdmin";
import UserManagement from "./Admin/HomePageAdmin/UserManagement/UserManagement";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePages />
              </Layout>
            }
          />
          <Route
            path="/title/:id"
            element={
              <Layout>
                <JobTitle />
              </Layout>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <JobDetail />
              </Layout>
            }
          />
          <Route
            path="/admin"
            element={
              <LayoutAdmin>
                <HomePageAdmin />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/user"
            element={
              <LayoutAdmin>
                <UserManagement />
              </LayoutAdmin>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
