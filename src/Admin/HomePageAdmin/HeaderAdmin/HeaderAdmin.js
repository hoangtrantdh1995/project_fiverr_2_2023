import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { setUserInfo } from "../../../redux_tookit/userSlice";

export default function HeaderAdmin() {
  let user = useSelector((state) => state.userSlice.user);

  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(setUserInfo(null));
  };

  const renderMenu = () => {
    return (
      <ul className="grid grid-cols-4 text-center ">
        <NavLink to="/admin/user">
          <li className="hover:text-green-500">Manage User</li>
        </NavLink>
        <li className="hover:text-green-500">Manage Job</li>
        <li className="hover:text-green-500">Manage JobType</li>
        <li className="hover:text-green-500">Manage Service</li>
      </ul>
    );
  };

  const renderLogo = () => {
    return (
      <div>
        <svg
          width="89"
          height="27"
          viewBox="0 0 89 27"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g fill="#262626">
            <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
          </g>
          <g className="dots_search" fill="#1dbf73">
            <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
          </g>
        </svg>
      </div>
    );
  };

  const renderContent = () => {
    if (user) {
      return (
        <div>
          <div className="flex">
            <img
              className="w-10 h-10 mr-4 rounded-full  ring-offset-4"
              src={user?.user?.avatar}
              alt=""
            />
            <button onClick={handleLogOut}>Đăng xuất</button>
          </div>
        </div>
      );
    }
  };

  return (
    <div className="bg-slate-100">
      <div className="container mx-auto flex justify-between items-center px-2 py-6 border-b-[2px]">
        <div>{renderLogo()}</div>
        <div>{renderContent()}</div>
      </div>
      <div className="container mx-auto pb-5 mt-5">{renderMenu()}</div>
    </div>
  );
}
