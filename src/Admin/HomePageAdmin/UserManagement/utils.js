import { Tag } from "antd";

export const userColums = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Full name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Phone",
    dataIndex: "phone",
    key: "phone",
  },
  {
    title: "Role",
    dataIndex: "role",
    key: "role",
    render: (text) => {
      if (text == "ADMIN") {
        return <Tag color="red">Admin</Tag>;
      } else {
        return <Tag color="blue">User</Tag>;
      }
    },
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
  },
];
