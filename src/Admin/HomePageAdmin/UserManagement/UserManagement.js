import { Button, message, Table } from "antd";
import React, { useEffect, useState } from "react";
import withAuth from "../../../HOC/WithAuthention/WithAuthention";
import {
  deleteUser,
  getUserList,
} from "../../../service/admin/userManagementService";
import { userColums } from "./utils";

function UserManagement() {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let fetchUserList = () => {
      getUserList()
        .then((res) => {
          console.log(res);
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              key: item.id,
              action: (
                <>
                  <div className="space-x-5">
                    <Button className="bg-blue-600 text-white">Edit</Button>
                    <Button
                      type="primary"
                      danger
                      onClick={() => {
                        handleRemoverUser(item.id);
                      }}
                    >
                      Delete
                    </Button>
                  </div>
                </>
              ),
            };
          });
          setUserArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    fetchUserList();

    let handleRemoverUser = (id) => {
      deleteUser(id)
        .then((res) => {
          console.log(res);
          message.success("Successful delete");

          fetchUserList();
        })
        .catch((err) => {
          console.log(err);
          message.error("Delete failed");
        });
    };

    let editUser = () => {};
  }, []);

  return (
    <div className="container mx-auto border border-gray-300 rounded">
      <Table columns={userColums} dataSource={userArr} />
    </div>
  );
}

export default withAuth(UserManagement);
