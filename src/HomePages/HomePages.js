import React from "react";
import IconMarketplace from "../components/IconMarketplace/IconMarketplace";
import ReactCarousel from "../components/ReactCarousel/ReactCarousel";
import Selling from "../components/Selling/Selling";
import ServiceCard from "../components/ServiceCard/ServiceCard";
import Slider from "../components/Slider/Slider";
import Trusted from "../components/Trusted/Trusted";

export default function HomePages() {
  return (
    <div>
      <Slider />
      <Trusted />
      <ReactCarousel />
      <Selling />
      <ServiceCard />
      <IconMarketplace />
    </div>
  );
}
