import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getJobDetail } from "../../service/jobService";

export default function JobDetail() {
  let params = useParams();
  const [jobDetail, setJobDetail] = useState();

  useEffect(() => {
    getJobDetail(params.id)
      .then((res) => {
        console.log(res);
        setJobDetail(res.data.content);
        console.log("res.data.content: ", res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [params.id]);

  const rederJobDetail = () =>
    jobDetail?.map((item, index) => {
      return (
        <div key={index} className="w-[90%] pb-36">
          <div className="w-full bg-white border border-gray-200 rounded-lg shadow">
            <div>
              <img className="w-full" src={item.congViec.hinhAnh} alt="" />
            </div>
            <div className="p-5 border-b-[1px]">
              <div className="flex">
                <div>
                  <img className="rounded-full w-16" src={item.avatar} alt="" />
                </div>
                <div className="pl-3">
                  <h2 className="text-2xl font-bold tracking-tight text-gray-800">
                    {item.tenNguoiTao}
                  </h2>
                  <p className="text-gray-500">
                    level {item.congViec.saoCongViec} Seller
                  </p>
                </div>
              </div>
              <p className="mt-4 text-gray-500">{item.congViec.tenCongViec}</p>
              <div className="pt-4 flex items-center">
                <span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 1792 1792"
                    width="15"
                    height="15"
                  >
                    <path
                      fill="#ffbe5b"
                      d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                    ></path>
                  </svg>
                </span>
                <span className="text-[#ffbe5b] ml-2">
                  {item.congViec.saoCongViec}
                </span>
                <span className="text-gray-500 ml-1">
                  ({item.congViec.danhGia})
                </span>
              </div>
            </div>
            <div className="flex justify-between items-center p-3">
              <span>
                <svg
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill="#a1a1aa"
                    d="M14.4469 1.95625C12.7344 0.496875 10.1875 0.759375 8.61561 2.38125L7.99999 3.01562L7.38436 2.38125C5.81561 0.759375 3.26561 0.496875 1.55311 1.95625C-0.409388 3.63125 -0.512513 6.6375 1.24374 8.45312L7.29061 14.6969C7.68124 15.1 8.31561 15.1 8.70624 14.6969L14.7531 8.45312C16.5125 6.6375 16.4094 3.63125 14.4469 1.95625Z"
                  ></path>
                </svg>
              </span>
              <div>
                <div className="flex items-baseline">
                  <p className="text-[10px] text-zinc-500">STARTING AT</p>
                  <span className="text-lg ml-2">
                    US${item.congViec.giaTien}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });

  return <div className="job_detail container mx-auto">{rederJobDetail()}</div>;
}
