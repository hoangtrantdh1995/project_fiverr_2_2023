import React from "react";

export default function JobTitelDetail({ job }) {
  const renderWorkGroup = () => (
    <div>
      {job.dsChiTietLoai.map((item, index) => {
        return (
          <div
            key={index}
            className="hover:text-green-500 py-2 pl-8 text-gray-500"
          >
            {item.tenChiTiet}
          </div>
        );
      })}
    </div>
  );

  return <>{renderWorkGroup()}</>;
}
