import React from "react";

export default function ServicesRelated() {
  return (
    <div className="service_related">
      <h2>Services Related To Digital Marketing</h2>
      <div className="tags">
        <span>Minimalist logo design</span>
        <span>Signature logo design</span>
        <span>Mascot logo design</span>
        <span>3d logo design</span>
        <span>Hand drawn logo design</span>
        <span>Vintage logo design</span>
        <span>Remove background</span>
        <span>Photo restoration</span>
        <span>Photo retouching</span>
        <span>Image resize</span>
        <span>Product label design</span>
        <span>Custom twitch overlay</span>
        <span>Custom twitch emotes</span>
        <span>Gaming logo</span>
        <span>Children book illustration</span>
      </div>
    </div>
  );
}
