import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getJobTypeDetails } from "../../service/jobService";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Logo_design from "../../imgFiverr/Logo design_2x.png";
import Architecture from "../../imgFiverr/Architecture _ Interior Design_2x.png";
import Illustration from "../../imgFiverr/Illustration_2x.png";
import Photoshop_Editing from "../../imgFiverr/Photoshop Editing_2x.png";
import T_Shirts from "../../imgFiverr/T-Shirts _ Merchandise_2x.png";
import Product_industrial from "../../imgFiverr/Product _ industrial design.png";
import Social_Media from "../../imgFiverr/Social Media Design_2x.png";
import Nft_Art from "../../imgFiverr/Nft Art (1).png";
import JobTitelDetail from "./JobTitelDetail";
import ServicesRelated from "./ServicesRelated";
import VideoFiverrWorks from "../../components/Video/VideoFiverrWorks";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        right: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        left: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

export default function JobTitle() {
  let params = useParams();
  const [jobTitle, setJobTitle] = useState();

  useEffect(() => {
    getJobTypeDetails(params.id)
      .then((res) => {
        console.log(res);
        setJobTitle(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [params.id]);

  const settings = {
    // dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  const renderJobTitle = () =>
    jobTitle?.map((item, index) => {
      return (
        <div className="job_title" key={index}>
          <h2 className="text-2xl py-10 pl-8">
            Most popular in {item.tenLoaiCongViec}
          </h2>
          <Slider className="" {...settings}>
            <div className="job_title-item">
              <img className="h-12" src={Logo_design} alt="" />
              <p className="px-3 item_hover">Minimallst Logo Design</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Architecture} alt="" />
              <p className="px-3 item_hover">Architecture & Interior Design</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Illustration} alt="" />
              <p className="px-3 item_hover">Illustration</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Photoshop_Editing} alt="" />
              <p className="px-3 item_hover">Image Editing</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={T_Shirts} alt="" />
              <p className="px-3 item_hover">T-Shirts & Merchandise</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Product_industrial} alt="" />
              <p className="px-3 item_hover">Industrial & Product Design</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Social_Media} alt="" />
              <p className="px-3 item_hover">Social Media Design</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
            <div className="job_title-item">
              <img className="h-12" src={Nft_Art} alt="" />
              <p className="px-3 item_hover">NFT Art</p>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                xmlns="http://www.w3.org/2000/svg"
                className="item_hover"
              >
                <path d="M9.92332 2.96885C9.63854 2.66807 9.1768 2.66807 8.89202 2.96885C8.60723 3.26963 8.60723 3.75729 8.89202 4.05807L11.6958 7.01931H1.48616C1.08341 7.01931 0.756918 7.36413 0.756918 7.7895C0.756918 8.21487 1.08341 8.5597 1.48616 8.5597H11.8436L8.89202 11.677C8.60723 11.9778 8.60723 12.4654 8.89202 12.7662C9.1768 13.067 9.63854 13.067 9.92332 12.7662L14.0459 8.41213C14.3307 8.11135 14.3307 7.62369 14.0459 7.32291L13.977 7.25011C13.9737 7.24661 13.9704 7.24315 13.9671 7.23972L9.92332 2.96885Z"></path>
              </svg>
            </div>
          </Slider>
          <h2 className="text-2xl pt-14 pb-5 pl-8">
            Explore {item.tenLoaiCongViec}
          </h2>
          <div className="grid grid-cols-4">
            {item.dsNhomChiTietLoai.map((item, index) => {
              return (
                <div key={index}>
                  <img className="w-[326px] pl-8" src={item.hinhAnh} alt="" />
                  <p className="text-black text-lg font-medium pt-6  pl-8">
                    {item.tenNhom}
                  </p>
                  <JobTitelDetail job={item} />
                </div>
              );
            })}
          </div>
        </div>
      );
    });

  return (
    <div className="container mx-auto">
      <div>
        <VideoFiverrWorks />
      </div>
      <div>{renderJobTitle()}</div>
      <div>
        <ServicesRelated />
      </div>
    </div>
  );
}
