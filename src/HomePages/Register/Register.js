import React, { useState } from "react";
import {
  LockFilled,
  UnlockFilled,
  PhoneFilled,
  MailFilled,
  EyeFilled,
} from "@ant-design/icons";
import { NavLink, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { postSingup } from "../../service/loginService";
import { message } from "antd";

export default function Register({ handleSwichToLogin }) {
  const regexName = /^\D{0,}$/;
  const regexPassword =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/;
  const regexPhone =
    /^(0)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/;

  const [passwordShow, setPasswordShow] = useState(false);
  const handlePasswordShow = () => {
    setPasswordShow(!passwordShow);
  };

  const [passwordConfirmShow, setpasswordConfirmShow] = useState(false);
  const handlePasswordConfirmShow = () => {
    setpasswordConfirmShow(!passwordConfirmShow);
  };

  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      passwordConfirm: "",
      phone: "",
      birthday: "",
      gender: true,
    },
    validationSchema: Yup.object().shape({
      name: Yup.string()
        .required("Full name is not blank!")
        .matches(regexName, "Name is not in the correct format!"),
      email: Yup.string()
        .required("Email is not blank!")
        .email("Email invalidate!"),
      password: Yup.string()
        .required("Password is not blank!")
        .matches(
          regexPassword,
          "Password must be at least 6 characters. Includes: special characters, uppercase letters, lowercase letters and numbers!"
        ),
      passwordConfirm: Yup.string()
        .required("PasswordConfirm is not blank!")
        .oneOf([Yup.ref("password")], "Password must match!"),
      phone: Yup.string()
        .required("Phone number is not blank!")
        .matches(regexPhone, "Incorrect phone number!"),
      birthday: Yup.string().required("Date of birth is not blank"),
      gender: Yup.string().required("Choose gender!"),
    }),

    onSubmit: (values) => {
      console.log("values: ", values);
      postSingup(values)
        .then((res) => {
          console.log(res);
          message.success("Sign Up Success");

          setTimeout(() => {
            handleSwichToLogin();
          }, 1000);
        })
        .catch((err) => {
          console.log(err);
          message.error(err.response.data.content);
        });
    },
  });

  return (
    <div className="flex flex-col max-w-md p-6 rounded-md sm:p-10 bg-white dark:text-black">
      <div className="mb-8 text-center">
        <h1 className="my-3 text-4xl font-bold">Sign up</h1>
        <p className="text-sm dark:text-gray-400">
          Register to become a member
        </p>
      </div>
      <form onSubmit={formik.handleSubmit}>
        <div className="form-group mb-6 flex items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-7 h-7 mr-3 text-green-600"
          >
            <path
              fillRule="evenodd"
              d="M7.5 6a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zM3.751 20.105a8.25 8.25 0 0116.498 0 .75.75 0 01-.437.695A18.683 18.683 0 0112 22.5c-2.786 0-5.433-.608-7.812-1.7a.75.75 0 01-.437-.695z"
              clipRule="evenodd"
            />
          </svg>
          <input
            name="name"
            type="text"
            className="form-control
block
w-full
pl-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="name"
            value={formik.values.name}
            placeholder="Full name"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.name && formik.touched.name ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.name}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center">
          <MailFilled
            style={{
              fontSize: "24px",
              paddingRight: "12px",
              color: "rgb(22 163 74 / var(--tw-text-opacity))",
            }}
          />
          <input
            name="email"
            type="email"
            className="form-control
block
w-full
pl-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="email"
            value={formik.values.email}
            placeholder="Enter email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.email && formik.touched.email ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.email}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center relative">
          <LockFilled
            style={{
              fontSize: "24px",
              paddingRight: "12px",
              color: "rgb(22 163 74 / var(--tw-text-opacity))",
            }}
          />
          <input
            name="password"
            type={passwordShow ? "text" : "password"}
            className="form-control
w-full
px-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="password"
            value={formik.values.password}
            placeholder="Password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <EyeFilled
            onClick={handlePasswordShow}
            className="absolute ml-[345px] text-gray-600 text-lg"
          />
          {formik.errors.password && formik.touched.password ? (
            <p className="absolute mt-[70px] pl-10 text-red-500 text-xs">
              {formik.errors.password}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center relative">
          <UnlockFilled
            style={{
              fontSize: "24px",
              paddingRight: "12px",
              color: "rgb(22 163 74 / var(--tw-text-opacity))",
            }}
          />
          <input
            name="passwordConfirm"
            type={passwordConfirmShow ? "text" : "password"}
            className="form-control
block
w-full
pl-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="passwordConfirm"
            placeholder="Repeat password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <EyeFilled
            onClick={handlePasswordConfirmShow}
            className="absolute ml-[345px] text-gray-600 text-lg"
          />
          {formik.errors.passwordConfirm && formik.touched.passwordConfirm ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.passwordConfirm}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center">
          <PhoneFilled
            style={{
              fontSize: "24px",
              paddingRight: "12px",
              color: "rgb(22 163 74 / var(--tw-text-opacity))",
            }}
          />
          <input
            name="phone"
            className="form-control
block
w-full
pl-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="phone"
            value={formik.values.phone}
            placeholder="Number phone"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.phone && formik.touched.phone ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.phone}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-7 h-7 mr-3 text-green-600"
          >
            <path d="M15 1.784l-.796.796a1.125 1.125 0 101.591 0L15 1.784zM12 1.784l-.796.796a1.125 1.125 0 101.591 0L12 1.784zM9 1.784l-.796.796a1.125 1.125 0 101.591 0L9 1.784zM9.75 7.547c.498-.02.998-.035 1.5-.042V6.75a.75.75 0 011.5 0v.755c.502.007 1.002.021 1.5.042V6.75a.75.75 0 011.5 0v.88l.307.022c1.55.117 2.693 1.427 2.693 2.946v1.018a62.182 62.182 0 00-13.5 0v-1.018c0-1.519 1.143-2.829 2.693-2.946l.307-.022v-.88a.75.75 0 011.5 0v.797zM12 12.75c-2.472 0-4.9.184-7.274.54-1.454.217-2.476 1.482-2.476 2.916v.384a4.104 4.104 0 012.585.364 2.605 2.605 0 002.33 0 4.104 4.104 0 013.67 0 2.605 2.605 0 002.33 0 4.104 4.104 0 013.67 0 2.605 2.605 0 002.33 0 4.104 4.104 0 012.585-.364v-.384c0-1.434-1.022-2.7-2.476-2.917A49.138 49.138 0 0012 12.75zM21.75 18.131a2.604 2.604 0 00-1.915.165 4.104 4.104 0 01-3.67 0 2.604 2.604 0 00-2.33 0 4.104 4.104 0 01-3.67 0 2.604 2.604 0 00-2.33 0 4.104 4.104 0 01-3.67 0 2.604 2.604 0 00-1.915-.165v2.494c0 1.036.84 1.875 1.875 1.875h15.75c1.035 0 1.875-.84 1.875-1.875v-2.494z" />
          </svg>
          <input
            name="birthday"
            type="date"
            className="form-control
block
w-full
pl-3
py-1.5
text-base
font-normal
text-gray-700
bg-white bg-clip-padding
border border-solid border-gray-300
rounded
transition
ease-in-out
m-0
focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
            id="birthday"
            placeholder="Birth day"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.birthday && formik.touched.birthday ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.birthday}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="form-group mb-6 mt-8 flex items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-7 h-7 mr-3 text-green-600"
          >
            <path d="M4.5 6.375a4.125 4.125 0 118.25 0 4.125 4.125 0 01-8.25 0zM14.25 8.625a3.375 3.375 0 116.75 0 3.375 3.375 0 01-6.75 0zM1.5 19.125a7.125 7.125 0 0114.25 0v.003l-.001.119a.75.75 0 01-.363.63 13.067 13.067 0 01-6.761 1.873c-2.472 0-4.786-.684-6.76-1.873a.75.75 0 01-.364-.63l-.001-.122zM17.25 19.128l-.001.144a2.25 2.25 0 01-.233.96 10.088 10.088 0 005.06-1.01.75.75 0 00.42-.643 4.875 4.875 0 00-6.957-4.611 8.586 8.586 0 011.71 5.157v.003z" />
          </svg>
          <select
            name="gender"
            id="gender"
            required
            onChange={formik.handleChange}
            className="block
      w-full
      pl-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-green-500 focus:outline-none"
          >
            <option selected>Choose gender</option>
            <option value={true}>Male</option>
            <option value={false}>Female</option>
          </select>
          {formik.errors.gender && formik.touched.gender ? (
            <p className="absolute mt-16 pl-10 text-red-500 text-xs">
              {formik.errors.gender}
            </p>
          ) : (
            ""
          )}
        </div>
        <div className="flex items-start mb-6 ml-9">
          <div className="flex items-center h-5">
            <input
              id="terms"
              aria-describedby="terms"
              type="checkbox"
              className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
              required
            />
          </div>
          <div className="ml-3 text-sm">
            <label htmlFor="terms" className="font-normal">
              I accept the{" "}
              <a
                className="font-normal hover:underline text-green-500"
                href="#"
              >
                Terms and Conditions
              </a>
            </label>
          </div>
        </div>
        <button
          type="submit"
          className="
w-full
px-6
py-2.5
bg-green-600
text-white
font-medium
text-xs
leading-tight
uppercase
rounded
shadow-md
hover:bg-green-700 hover:shadow-lg
focus:bg-green-700 focus:shadow-lg focus:outline-none focus:ring-0
active:bg-green-800 active:shadow-lg
transition
duration-150
ease-in-out"
        >
          Create an account
        </button>
        <p className="text-gray-800 mt-6 text-center">
          Already a member?
          <a
            onClick={handleSwichToLogin}
            href="#"
            className="font-medium hover:underline text-green-500"
          >
            Login here
          </a>
        </p>
      </form>
    </div>
  );
}
