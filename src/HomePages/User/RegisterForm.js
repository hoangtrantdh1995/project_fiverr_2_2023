import { Modal } from "antd";
import React from "react";
import Register from "../Register/Register";

export default function RegisterForm({ open, submit, close, showLogin }) {
  return (
    <Modal
      open={open}
      onOk={submit}
      onCancel={close}
      destroyOnClose={false}
      okType={"default"}
      footer={null}
      closable={false}
    >
      <Register handleSwichToLogin={showLogin} />
    </Modal>
  );
}
