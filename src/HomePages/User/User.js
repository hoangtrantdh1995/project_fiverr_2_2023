import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setUserInfo } from "../../redux_tookit/userSlice";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

export default function User() {
  let user = useSelector((state) => state.userSlice.user);

  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(setUserInfo(null));
  };

  const [isModalRegister, setIsModalRegister] = useState(false);
  const [isModalLogin, setIsModalLogin] = useState(false);
  const showModalRegister = () => {
    setIsModalRegister(true);
    setIsModalLogin(false);
  };

  const handleCancelRegister = () => {
    setIsModalRegister(false);
  };

  const showModalLogin = () => {
    setIsModalLogin(true);
    setIsModalRegister(false);
  };

  const handleCancelLogin = () => {
    setIsModalLogin(false);
  };

  const renderContent = () => {
    if (user) {
      return (
        <div className="flex">
          <img
            className="w-10 h-10 mr-2 rounded-full  ring-offset-4"
            src={user?.user?.avatar}
            alt=""
          />
          <button onClick={handleLogOut}>Đăng xuất</button>
        </div>
      );
    } else {
      return (
        <>
          <LoginForm
            open={isModalLogin}
            close={handleCancelLogin}
            showRegister={showModalRegister}
          />
          <RegisterForm
            open={isModalRegister}
            close={handleCancelRegister}
            showLogin={showModalLogin}
          />
          <div className="flex justify-center items-center ">
            <p className="pr-4 hover:text-green-500">
              <button onClick={showModalLogin}>Sign In</button>
            </p>
            <p className="hover:text-green-500 rounded border border-green-500 ">
              <button onClick={showModalRegister} className="mx-3 my-1">
                Join
              </button>
            </p>
          </div>
        </>
      );
    }
  };

  return <div>{renderContent()}</div>;
}
