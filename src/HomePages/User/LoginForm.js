import { Modal } from "antd";
import React from "react";
import Login from "../Login/Login";

export default function LoginForm({ open, submit, close, showRegister }) {
  return (
    <Modal
      open={open}
      onOk={submit}
      onCancel={close}
      destroyOnClose={false}
      okType={"default"}
      footer={null}
      closable={false}
    >
      <Login handleSwitchToRegister={showRegister} />
    </Modal>
  );
}
