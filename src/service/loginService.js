import { https } from "./configURL";

export const postSingin = (data) => {
  return https.post(`/api/auth/signin`, data);
};

export const postSingup = (data) => {
  return https.post(`/api/auth/signup`, data);
};
