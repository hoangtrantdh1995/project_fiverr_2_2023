import { https } from "../configURL";

export const getUserList = () => {
  return https.get(`/api/users`);
};

export const putUser = (id) => {
  return https.put(`/api/users/${id}`);
};

export const deleteUser = (id) => {
  return https.delete(`/api/users?id=${id}`);
};
