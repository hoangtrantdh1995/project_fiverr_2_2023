import { https } from "./configURL";

export const getUser = (data) => {
  return https(`api/users`, data);
};
