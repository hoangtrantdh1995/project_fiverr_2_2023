import { https } from "./configURL";

export const getMenuJob = () => {
  return https.get(`/api/cong-viec/lay-menu-loai-cong-viec`);
};
export const getJobTypeDetails = (data) => {
  return https.get(`/api/cong-viec/lay-chi-tiet-loai-cong-viec/${data}`);
};
export const getJobDetail = (data) => {
  return https.get(`/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${data}`);
};
