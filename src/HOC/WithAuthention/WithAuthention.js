import { message } from "antd";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const withAuth = (WrappedComponent) => {
  return (props) => {
    const navigate = useNavigate();
    let user = useSelector((state) => state.userSlice.user);
    console.log("user: ", user);
    useEffect(() => {
      if (user?.user.role !== "ADMIN") {
        navigate("/");
        message.error("you don't have enough access rights");
      }
    }, []);
    return <WrappedComponent {...props} />;
  };
};

export default withAuth;
