import React from "react";
import HeaderAdmin from "../../Admin/HomePageAdmin/HeaderAdmin/HeaderAdmin";

export default function LayoutAdmin({ children }) {
  return (
    <>
      <HeaderAdmin />
      <div>{children}</div>
    </>
  );
}
