import React from "react";
import SliderCarousel from "./SliderCarousel";

export default function Slider() {
  return (
    <div className="homepage">
      <div className="-z-10">
        <div className=" bg-gray-100">
          <SliderCarousel className="relative" />
        </div>
      </div>
    </div>
  );
}
