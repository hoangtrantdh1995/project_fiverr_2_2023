import React from "react";
import { Carousel } from "antd";
import background1 from "../../imgFiverr/1.png";
import background2 from "../../imgFiverr/2.png";
import background3 from "../../imgFiverr/3.png";
import background4 from "../../imgFiverr/4.png";
import background5 from "../../imgFiverr/5.png";
import Search from "../Search/Search";

export default function SliderCarousel() {
  const contentStyle = {
    height: "100%",
    width: "100%",
    lineHeight: "160px",
    textAlign: "center",
    backgroundPosition: "",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };

  return (
    <div className="container mx-auto">
      <Carousel autoplay>
        <div>
          <img style={contentStyle} src={background1} alt="" />
        </div>
        <div>
          <img style={contentStyle} src={background2} alt="" />
        </div>
        <div>
          <img style={contentStyle} src={background3} alt="" />
        </div>
        <div>
          <img style={contentStyle} src={background4} alt="" />
        </div>
        <div>
          <img style={contentStyle} src={background5} alt="" />
        </div>
      </Carousel>
      <Search />
    </div>
  );
}
