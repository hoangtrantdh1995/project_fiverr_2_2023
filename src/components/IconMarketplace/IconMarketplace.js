import React from "react";
import Graphics_design from "../../imgFiverr/graphics-design.svg";
import Online_marketing from "../../imgFiverr/online-marketing.svg";
import Writing_translation from "../../imgFiverr/writing-translation.svg";
import Video_animation from "../../imgFiverr/video-animation.svg";
import Music_audio from "../../imgFiverr/music-audio.svg";
import Programming from "../../imgFiverr/programming.svg";
import Business from "../../imgFiverr/business.svg";
import Lifestyle from "../../imgFiverr/lifestyle.svg";
import Data from "../../imgFiverr/data.svg";
import Photography from "../../imgFiverr/photography.svg";

export default function IconMarketplace() {
  return (
    <div className="container mx-auto px-4 pb-32 icon-marketplace">
      <h2>Explore the marketplace</h2>
      <ul className="grid grid-cols-5 gap-11">
        <li>
          <a href="">
            <img src={Graphics_design} alt="" />
          </a>
          <p>Graphics & Design</p>
        </li>

        <li>
          <a href="">
            <img src={Online_marketing} alt="" />
          </a>
          <p>Digital Marketing</p>
        </li>

        <li>
          <a href="">
            <img src={Writing_translation} alt="" />
          </a>
          <p>Writing & Translation</p>
        </li>

        <li>
          <a href="">
            <img src={Video_animation} alt="" />
          </a>
          <p>Video & Animation</p>
        </li>

        <li>
          <a href="">
            <img src={Music_audio} alt="" />
          </a>
          <p>Music & Audio</p>
        </li>

        <li>
          <a href="">
            <img src={Programming} alt="" />
          </a>
          <p>Programming & Tech</p>
        </li>

        <li>
          <a href="">
            <img src={Business} alt="" />
          </a>
          <p>Business</p>
        </li>

        <li>
          <a href="">
            <img src={Lifestyle} alt="" />
          </a>
          <p>Lifestyle</p>
        </li>

        <li>
          <a href="">
            <img src={Data} alt="" />
          </a>
          <p>Data</p>
        </li>

        <li>
          <a href="">
            <img src={Photography} alt="" />
          </a>
          <p>Photography</p>
        </li>
      </ul>
    </div>
  );
}
