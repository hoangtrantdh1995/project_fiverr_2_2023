import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import VideoAntdModal1 from "../Video/VideoAntdModal1";
import VideoAntdModal2 from "../Video/VideoAntdModal2";
import VideoAntdModal3 from "../Video/VideoAntdModal3";
import VideoAntdModal4 from "../Video/VideoAntdModal4";
import rooted from "../../imgFiverr/rooted.png";
import haerfes from "../../imgFiverr/haerfes.png";
import lavender from "../../imgFiverr/lavender.png";
import naadampng from "../../imgFiverr/naadampng.png";
function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        right: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        left: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

export default function ServiceCard() {
  const settings = {
    // dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <div className="serviceCard px-4 container mx-auto relative">
      <Slider {...settings}>
        <div className="displayStyle">
          <div className="flex w-2/5">
            <VideoAntdModal1 />
          </div>
          <div className="flex flex-col w-3/5 px-4">
            <div className="service-Img flex items-center flex-start">
              <h5 className="border-r-2">Kay Kim, Co-Founder</h5>
              <img src={rooted} alt="" />
            </div>
            <div>
              <p>
                "It's extremely exciting that Fiverr has freelancers from all
                over the world — it broadens the talent pool. One of the best
                things about Fiverr is that while we're sleeping, someone's
                working."
              </p>
            </div>
          </div>
        </div>
        <div className="displayStyle">
          <div className="flex w-2/5">
            <VideoAntdModal2 />
          </div>
          <div className="service-Img flex flex-col w-3/5 px-4">
            <div className="flex items-center flex-start">
              <h5 className="border-r-2">
                Caitlin Tormey, Chief Commercial Officer
              </h5>
              <img src={naadampng} alt="" />
            </div>
            <div>
              <p>
                "We used Fiverr for SEO, our logo, website, copy, animated
                videos — literally everything. It was like working with a human
                right next to you versus being across the world."
              </p>
            </div>
          </div>
        </div>
        <div className="displayStyle">
          <div className="flex w-2/5">
            <VideoAntdModal3 />
          </div>
          <div className="flex flex-col w-3/5 px-4">
            <div className="service-Img flex items-center flex-start">
              <h5 className="border-r-2">
                Brighid Gannon (DNP, PMHNP-BC), Co-Founder
              </h5>
              <img src={lavender} alt="" />
            </div>
            <div>
              <p>
                "We used Fiverr for SEO, our logo, website, copy, animated
                videos — literally everything. It was like working with a human
                right next to you versus being across the world."
              </p>
            </div>
          </div>
        </div>
        <div className="displayStyle">
          <div className="flex w-2/5">
            <VideoAntdModal4 />
          </div>
          <div className="flex flex-col w-3/5 px-4">
            <div className="service-Img flex items-center flex-start">
              <h5 className="border-r-2">Tim and Dan Joo, Co-Founders</h5>
              <img src={haerfes} alt="" />
            </div>
            <div>
              <p>
                "When you want to create a business bigger than yourself, you
                need a lot of help. That's what Fiverr does."
              </p>
            </div>
          </div>
        </div>
      </Slider>
    </div>
  );
}
