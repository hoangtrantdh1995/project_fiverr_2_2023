import React from "react";

export default function Search() {
  return (
    <div className="flex justify-center items-center absolute top-[160px] pl-7">
      <div>
        <div className="text-white">
          <h1 className="text-[40px] ">
            Find the perfect <i className="freelance ">freelance</i>
            <br />
            services for your business
          </h1>
          <div className="input_homepgae flex justify-center items-center pt-6 ">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="h-12 w-10 icon_homepgae"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
              />
            </svg>
            <input
              className="search_homepage font-extralight h-12 w-[500px]"
              type="text_home_page"
              placeholder='Try "building mobile app"'
            />
            <button className="px-5 button_homepage text-[16px] py-2 w-auto h-12 text-white bg-[#1dbf73]">
              Search
            </button>
          </div>
          <div className="popular flex items-center pt-5">
            <h2 className="text-[16px]">Popular:</h2>
            <ul className="flex  ml-2 space-x-5 ">
              <li className="item_popular">
                <a href="">Website Design</a>
              </li>
              <li className="item_popular">
                <a href="">WordPress</a>
              </li>
              <li className="item_popular">
                <a href="">Logo Design</a>
              </li>
              <li className="item_popular">
                <a href="">Ai Services</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div>{""}</div>
    </div>
  );
}
