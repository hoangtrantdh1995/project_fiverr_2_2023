import React from "react";
import { Modal } from "antd";
import { useState } from "react";
import testimonial1 from "../../imgFiverr/testimonial1.png";

import iconPlay from "../../imgFiverr/iconPlay.png";

export default function VideoAntdModal1() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <button onClick={showModal} className="relative">
        <img src={testimonial1} alt="" className="h-full w-full" />
        <img
          src={iconPlay}
          alt=""
          className="absolute top-[40%] left-[45%] opacity-60 w-[15%]"
        />
      </button>
      <Modal
        className=" items-center flex justify-center"
        width={900}
        height="auto"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        destroyOnClose={true}
        okType={"default"}
        footer={null}
        closable={false}
      >
        <video controls autoplay={true} loop>
          <source
            src="https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/yja2ld5fnolhsixj3xxw"
            type="video/mp4"
          />
        </video>
      </Modal>
    </div>
  );
}
