import React, { useState } from "react";
import how_fiverr_works from "../../imgFiverr/how_fiverr_works.mp4";
import { Modal } from "antd";

export default function VideoFiverrWorks() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <div className="bg-green-900 w-full h-[250px] flex justify-center items-center">
        <button
          onClick={showModal}
          className="flex justify-center items-center mx-auto my-10 border border-white rounded"
        >
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="far"
            data-icon="circle-play"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 512 512"
            data-fa-i2svg=""
            className="text-white w-5 h-5 ml-4 mr-2"
          >
            <path
              fill="currentColor"
              d="M188.3 147.1C195.8 142.8 205.1 142.1 212.5 147.5L356.5 235.5C363.6 239.9 368 247.6 368 256C368 264.4 363.6 272.1 356.5 276.5L212.5 364.5C205.1 369 195.8 369.2 188.3 364.9C180.7 360.7 176 352.7 176 344V167.1C176 159.3 180.7 151.3 188.3 147.1V147.1zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z"
            ></path>
          </svg>
          <h5 className="text-sm text-white mr-4 my-2">How Fiverr Works</h5>
        </button>
        <Modal
          className=" items-center flex justify-center"
          width={900}
          height="auto"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          destroyOnClose={true}
          okType={"default"}
          footer={null}
          closable={false}
        >
          <video controls autoplay={true} loop>
            <source src={how_fiverr_works} type="video/mp4" />
          </video>
        </Modal>
      </div>
    </>
  );
}
