import React from "react";
import facebook from "../../imgFiverr/fb.png";
import google from "../../imgFiverr/google.png";
import netflix from "../../imgFiverr/netflix.png";
import paypal from "../../imgFiverr/paypal.png";
import pg from "../../imgFiverr/pg.png";

export default function Trusted() {
  return (
    <div className="pb-24">
      <div className="bg-[#FAFAFA] py-5 flex justify-center trusted items-center text-[#B5B6BA] space-x-3">
        <p className="text-xl pr-10">Trusted by: </p>
        <img className="pr-14" src={facebook} alt="" />
        <img className="pr-14" src={google} alt="" />
        <img className="pr-14" src={netflix} alt="" />
        <img className="pr-14" src={pg} alt="" />
        <img className="pr-14" src={paypal} alt="" />
      </div>
    </div>
  );
}
