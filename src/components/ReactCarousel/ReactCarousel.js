import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import crs1 from "../../imgFiverr/crs1.png";
import crs2 from "../../imgFiverr/crs2.png";
import crs3 from "../../imgFiverr/crs3.png";
import crs4 from "../../imgFiverr/crs4.png";
import crs5 from "../../imgFiverr/crs5.png";
import crs6 from "../../imgFiverr/crs6.png";
import crs7 from "../../imgFiverr/crs7.png";
import crs8 from "../../imgFiverr/crs8.png";
import crs9 from "../../imgFiverr/crs9.png";
import crs10 from "../../imgFiverr/crs10.png";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        right: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        width: "48px",
        height: "48px",
        content: "",
        left: "0%",
        translate: "translateY(-50%)",
        zIndex: "5",
        borderRadius: "50%",
        boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
        border: "1px solid #ccc",
      }}
      onClick={onClick}
    />
  );
}

export default function ReactCarousel() {
  const settings = {
    // dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <div className="relative pb-24 container mx-auto">
      <h1 className="ml-4 pb-6 text-4xl">Popular professional services</h1>
      <Slider className="" {...settings}>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs1} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs2} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs3} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs4} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs5} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs6} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs7} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs8} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs9} alt="" />
        </div>
        <div className="px-4">
          <img className="block h-[345px] object-covers" src={crs10} alt="" />
        </div>
      </Slider>
    </div>
  );
}
