import React from "react";
import { NavLink } from "react-router-dom";
import CategoriesMenu from "../CategoriesMenu/CategoriesMenu";
import User from "../../HomePages/User/User";

export default function Header() {
  // item của dropdown Exlore
  const renderDropdown = () => {
    return (
      <div className="drop_down exlore exlore_dropdown">
        <button className="button_exlore">Exlore</button>
        <div className="content_dropdown shadow-2xl ">
          <div className="content_exlore flex justify-between  ">
            <div className="content_left ">
              <ul className="space-y-5 content_menu">
                <li>
                  <a href="#">
                    <h2>Discover</h2>
                    <p>Inspiring projects made on Fiverr</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Guides</h2>
                    <p>In-depth guides covering business topics</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Learn</h2>
                    <p>Professional online courses, led by experts</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Logo Maker</h2>
                    <p>Create your logo instantly</p>
                  </a>
                </li>
              </ul>
            </div>
            <div className="content_right ">
              <ul className="content_menu space-y-5">
                <li>
                  <a href="#">
                    <h2>Community</h2>
                    <p>Connect with Fiverr’s team and community</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Podcast</h2>
                    <p>Inside tips from top business minds</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Blog</h2>
                    <p>News, information and community stories</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <h2>Fiverr Workspace</h2>
                    <p>One place to manage your business</p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  };
  // nút search trong header
  const renderSearchInput = () => {
    return (
      <div className="checkbox flex items-center">
        <input
          type="text"
          className="search_box px-5 font-normal text-[17px] text-[#62646a]  h-10 w-[250px]   "
          placeholder="Find Service"
        />
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
          className="w-12 h-10 icon_search hover:bg-[#404145]   bg-[#222325] text-white "
        >
          <path
            fillRule="evenodd"
            d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z"
            clipRule="evenodd"
          />
        </svg>
      </div>
    );
  };
  // chi tiết công việc
  const renderMenu = () => {
    return (
      <div>
        <ul className="font-serif flex text-white justify-between items-center">
          <CategoriesMenu />
        </ul>
      </div>
    );
  };

  return (
    <header className="header z-10 fixed w-full divide-y">
      <div className="py-5 px-24">
        <div className="container mx-auto">
          <div className="flex justify-between items-center px-4">
            <svg
              width="89"
              height="27"
              viewBox="0 0 89 27"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g fill="#ffff">
                <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
              </g>
              <g className="dots_search" fill="#1dbf73">
                <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
              </g>
            </svg>
            <div className="input_search ">{renderSearchInput()}</div>
            <div className="header_right ">
              <nav className="drop_down ">
                <ul className="flex justify-center text-white items-center space-x-4">
                  <li className="main_business">
                    <NavLink to={"/"}>Fiverr Business</NavLink>
                  </li>
                  <div className="exlore">{renderDropdown()}</div>
                  <li className="modal_language flex duration-500">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 "
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418"
                      />
                    </svg>
                    <h1>English</h1>
                  </li>
                  <li>
                    <h1>US$ USD</h1>
                  </li>
                  <li>
                    <h1>Become a Seller</h1>
                  </li>
                  <li>
                    <User />
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <div className="header_menu mt-1 w-full py-2 container mx-auto">
        {renderMenu()}
      </div>
    </header>
  );
}
