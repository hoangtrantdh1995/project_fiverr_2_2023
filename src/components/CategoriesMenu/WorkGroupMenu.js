import React from "react";
import { NavLink } from "react-router-dom";

export default function WorkGroupMenu({ job }) {
  const renderWorkGroup = () => (
    <ul>
      {job.dsChiTietLoai.map((item, index) => {
        return (
          <NavLink key={index} to={`/detail/${item.id}`}>
            <li className="hover:text-green-500" href="">
              {item.tenChiTiet}
            </li>
          </NavLink>
        );
      })}
    </ul>
  );
  return <div>{renderWorkGroup()}</div>;
}
