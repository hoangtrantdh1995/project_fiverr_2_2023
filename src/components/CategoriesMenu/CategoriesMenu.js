import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { getMenuJob } from "../../service/jobService";
import WorkGroupMenu from "./WorkGroupMenu";

export default function CategoriesMenu() {
  const [menuJob, setmenuJob] = useState([]);

  useEffect(() => {
    getMenuJob()
      .then((res) => {
        console.log("res: ", res);
        setmenuJob(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderMenuJob = () => {
    return menuJob.map((item, index) => {
      return (
        <div key={index} className="text-black text-[18px] leading-9 menu_job ">
          <div className="job_type">
            <NavLink to={`/title/${item.id}`}>
              <div>{item.tenLoaiCongViec}</div>
            </NavLink>
            <div className="menu_job-item ">
              {item.dsNhomChiTietLoai.map((item, index) => {
                return (
                  <div key={index}>
                    <div className="text-black font-semibold">
                      {item.tenNhom}
                    </div>
                    <WorkGroupMenu job={item} />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  };

  return <>{renderMenuJob()}</>;
}
